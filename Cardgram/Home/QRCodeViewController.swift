//
//  QRCodeViewController.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 11/9/21.
//

import UIKit

class QRCodeViewController: ViewController {

    @IBOutlet weak var qrImageView: UIImageView!
    
    var qrCode: UIImage?
    var onScreen = true
    var rects: [UIImageView]?

    @IBOutlet weak var rect0: UIImageView!
    @IBOutlet weak var rect1: UIImageView!
    @IBOutlet weak var rect2: UIImageView!
    @IBOutlet weak var rect3: UIImageView!
    @IBOutlet weak var rect4: UIImageView!
    @IBOutlet weak var rect5: UIImageView!
    @IBOutlet weak var rect6: UIImageView!
    @IBOutlet weak var rect7: UIImageView!
    @IBOutlet weak var rect8: UIImageView!
    @IBOutlet weak var rect9: UIImageView!
    @IBOutlet weak var rect10: UIImageView!
    @IBOutlet weak var rect11: UIImageView!
    @IBOutlet weak var rect12: UIImageView!
    @IBOutlet weak var rect13: UIImageView!
    @IBOutlet weak var rect14: UIImageView!
    @IBOutlet weak var rect15: UIImageView!

    private let quotes = [
        "\"Is that a gram?\"",
        "\"Let's see Paul Allen's card.\"",
        "\"New card. What do you think?\"",
        "\"You ain't seen nothin' yet.\"",
        "\"Oh my god. It even has a watermark.\"",
        "\"Good coloring.\"",
        "\"I can't believe that Bryce prefers Van Patten's card to mine.\"",
        "\"Impressive. Very nice.\""
    ]
    @IBOutlet weak var quoteLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qrImageView.image = qrCode

        rects = [rect0, rect1, rect2, rect3, rect4, rect5, rect6, rect7, rect8, rect9, rect10, rect11, rect12, rect13, rect14, rect15]
        for r in rects! {
            r.alpha = 0.0
        }
    }

    // MARK: ANIMATION

    // begin animation recursion
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        onScreen = true
        quoteLabel.text = quotes[Int.random(in: 0..<quotes.count)]
        self.animateBackgroundRects()
    }

    // end animation recursion
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        onScreen = false
    }

    // asynchronously fade in/out a couple of indigo rectangles
    private func animateBackgroundRects() {
        if (onScreen) {
            let size = rects!.count
            let i = Int.random(in: 0..<size)
            
            let animateQueue = DispatchQueue.init(label: "qAnimate")
            animateQueue.async {
                DispatchQueue.main.async {
                    self.animateHelperR(size: size, index: i)
                }
                DispatchQueue.main.async {
                    self.animateHelperQ(size: size, index: i + 8)
                }
            }
        }
    }
    
    // fade in/out a rect for 6 seconds
    private func animateHelperR(size: Int, index: Int) {
        var r: UIImageView?

        UIView.animate(withDuration: 2.0, delay: 1.0, options: .curveEaseOut, animations: { [self] in
            r = rects![index]
            r!.alpha = 0.5

        }, completion: {
            (completed: Bool) -> Void in
            UIView.animate(withDuration: 2.0, delay: 1.0, options: .curveEaseIn, animations: {
                r!.alpha = 0.0
                
            }, completion: {
                (completed: Bool) -> Void in
                self.animateBackgroundRects()
            })
        })
    }
    
    // fade in/out a different rect for 10.5 seconds
    private func animateHelperQ(size: Int, index: Int) {
        var q: UIImageView?

        UIView.animate(withDuration: 5.0, delay: 1.0, options: .curveEaseOut, animations: { [self] in
            q = rects![index % (size - 1)]
            q!.alpha = 0.5
            
        }, completion: {
            (completed: Bool) -> Void in
            UIView.animate(withDuration: 3.5, delay: 1.0, options: .curveEaseIn, animations: {
                q!.alpha = 0.0
                
            }, completion: nil)
        })
    }

}
