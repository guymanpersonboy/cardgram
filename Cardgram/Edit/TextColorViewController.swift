//
//  TextColorViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 12/5/21.
//

import UIKit

class TextColorViewController: ViewController, UIColorPickerViewControllerDelegate {
    var cardEditorDelegate: EditViewController?

    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        cardEditorDelegate?.edit(textColor: viewController.selectedColor)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? UIColorPickerViewController {
            destination.delegate = self
        }
    }
}

