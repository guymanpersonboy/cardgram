//
//  CourtesyTitleViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/1/21.
//

import UIKit

// View controller allows the user to edit the professional name.
class ProfessionalNameViewController: ViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    var cardEditorDelegate: EditViewController?

    // Available list of titles.
    let courtesyTitles: [String] = ["", "Mr.", "Mrs.", "Ms.", "Dr.", "Sir", "Madam"]

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var courtesyTitlePicker: UIPickerView!
    @IBOutlet weak var firstNameInputGroup: InputGroup!
    @IBOutlet weak var middleNameInputGroup: InputGroup!
    @IBOutlet weak var lastNameInputGroup: InputGroup!

    override func viewDidLoad() -> Void {
        super.viewDidLoad()

        courtesyTitlePicker.dataSource = self
        courtesyTitlePicker.delegate = self
        firstNameInputGroup.textField.delegate = self
        middleNameInputGroup.textField.delegate = self
        lastNameInputGroup.textField.delegate = self

        firstNameInputGroup.textField.addTarget(self, action: #selector(firstNameTextFieldEditingChanged(_:)), for: .editingChanged)
        middleNameInputGroup.textField.addTarget(self, action: #selector(middleNameTextFieldEditingChanged(_:)), for: .editingChanged)
        lastNameInputGroup.textField.addTarget(self, action: #selector(lastNameTextFieldEditingChanged(_:)), for: .editingChanged)
        firstNameInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        middleNameInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        lastNameInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)

        firstNameInputGroup.textField.text = cardEditorDelegate?.cardView.firstName
        middleNameInputGroup.textField.text = cardEditorDelegate?.cardView.middleName
        lastNameInputGroup.textField.text = cardEditorDelegate?.cardView.lastName
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        courtesyTitlePicker.selectRow(cardEditorDelegate?.cardView.courtesyTitleIndex ?? 0, inComponent: 0, animated: true)
    }

    @objc func scrollToTop(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return courtesyTitles.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = UIFont(name: "WorkSans-Regular", size: label.font.pointSize)
        label.text = courtesyTitles[row]
        label.textAlignment = .center

        return label
    }

    // Set the professional name based on the combination of first, middle, last names and titles.
    private func updateFullProfessionalName() -> Void {
        let courtesySelected = courtesyTitlePicker.selectedRow(inComponent: 0)
        cardEditorDelegate?.cardView.setName(courtesyTitleIndex: courtesySelected, courtesyTitle: courtesyTitles[courtesySelected], firstName: firstNameInputGroup.textField.text!, middleName: middleNameInputGroup.textField.text!, lastName: lastNameInputGroup.textField.text!)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) -> Void {
        updateFullProfessionalName()
    }

    @objc func firstNameTextFieldEditingChanged(_ sender: Any) -> Void {
        updateFullProfessionalName()
    }

    @objc func middleNameTextFieldEditingChanged(_ sender: Any) -> Void {
        updateFullProfessionalName()
    }

    @objc func lastNameTextFieldEditingChanged(_ sender: Any) -> Void {
        updateFullProfessionalName()
    }
}
