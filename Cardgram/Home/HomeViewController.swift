//
//  HomeViewController.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 10/14/21.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage

class HomeViewController: ViewController, UIScrollViewDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    let db = Firestore.firestore()
    let storage = Storage.storage().reference()

    var firstName: String = ""

    // views
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var profilePic: UIImageView!
    // user labels
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var friendsCountLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    // identifiers
    let friendsSegueIdentifier = "HomeToFriendsSegue"
    let noFriendsSegueIdentifier = "NoFriendsSegue"
    // instance variables
    var friendsCount: Int!

    // refresh the screen with new data before displaying it
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showSpinner()
        let selfUid = Auth.auth().currentUser?.uid
        guard selfUid != nil else {
            return
        }
        loadProfile()
        loadLabels()
        db.collection("cards")
            .document(selfUid!)
            .getDocument { (document, error) in
                if let document = document, document.exists {
                    self.cardView.deserialize(serialization: document)
                }
            }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: LOAD CURRENT PROFILE
    
    // load profile pic from Firebase Storage
    private func loadProfile() {
        // get url from UserDefaults
        Storage.storage()
            .reference()
            .child("images/\(Auth.auth().currentUser!.uid)/profilepicture")
            .downloadURL { (url, error) in
                guard let url = url, error == nil else {
                    return
                }

                URLSession.shared.dataTask(with: url, completionHandler: { data, _, error in
                    guard let data = data, error == nil else {
                        return
                    }
                    DispatchQueue.main.async { [self] in
                        let image = UIImage(data: data)
                        self.maskCircle()
                        profilePic.image = image
                        self.hideSpinner()
                    }
                }).resume()
            }
    }
    
    // make square(* must to make circle),
    // resize(reduce the kilobyte) and
    // fix rotation.
    public func maskCircle() {
        profilePic.contentMode = UIView.ContentMode.scaleAspectFill
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.layer.masksToBounds = false
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
        profilePic.clipsToBounds = true
    }

    // update the labels on the Home Page
    private func loadLabels() {
        let db = Firestore.firestore()
        db.collection("users").document(Auth.auth().currentUser!.uid).getDocument {
            (document, error) in
            if let document = document, document.exists {
                let friendsCount: Int = document.get("friendsCount") as! Int
                self.friendsCountLabel.text = "\(friendsCount)"
                self.friendsCount = friendsCount

                let averageRating: Double = document.get("averageRating") as! Double
                self.ratingLabel.text = String(format: "%.1f", averageRating)

                self.firstName = document.get("firstName") as! String
                let lastName: String = document.get("lastName") as! String
                let emailAddress = document.get("emailAddress") as! String
                let bio: String = document.get("bio") as! String
                let date: String = document.get("joiningDate") as! String

                self.nameLabel.text = "\(self.firstName) \(lastName)"
                self.emailAddressLabel.text = "\(emailAddress)"
                self.bioLabel.text = "\(bio)"
                self.dateLabel.text = "\(date)"
            }
        }
    }

    // MARK: NAVIGATION
    
    @IBAction func friendsButtonPressed(_ sender: Any) {
        if self.friendsCount == 0 {
            performSegue(withIdentifier: noFriendsSegueIdentifier, sender: self)
        } else {
            performSegue(withIdentifier: friendsSegueIdentifier, sender: self)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? QRCodeViewController {
            destination.qrCode = generateQRCodeImage()
        }

        if let destination = segue.destination as? FriendsViewController {
            destination.uid = Auth.auth().currentUser!.uid
            destination.title = "Your Friends"
        }
    }
    
    // generate QR code from uid for segue
    private func generateQRCodeImage() -> UIImage {
        let filter = CIFilter(name: "CIQRCodeGenerator")
        if let text = Auth.auth().currentUser?.uid {
            let data = text.data(using: .ascii, allowLossyConversion: false)
            filter?.setValue(data, forKey: "InputMessage")
        } else {
            return UIImage(systemName: "xmark") ?? UIImage()
        }
        // remove blurriness
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        return UIImage(ciImage: (filter?.outputImage)!.transformed(by: transform))
    }
}
