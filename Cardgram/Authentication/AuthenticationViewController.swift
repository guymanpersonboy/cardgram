//
//  AuthenticationViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 10/13/21.
//

import UIKit

// Main view controller for authentication purposes.
// Allows the user to choose between registering and signing up.
class AuthenticationViewController: ViewController {
    private let quotes = [
        "Is that a gram?",
        "Let's see Paul Allen's card.",
        "New card. What do you think?",
        "You ain't seen nothin' yet.",
        "Oh my god. It even has a watermark.",
        "Good coloring.",
        "I can't believe that Bryce prefers Van Patten's card to mine.",
        "Impressive. Very nice.",
    ]

    private var displayedQuoteIndex = 0

    @IBOutlet private var quoteLabel: UILabel!

    private func hideQuoteLabel() {
        quoteLabel.alpha = 0
    }

    private func showQuoteLabel() {
        quoteLabel.alpha = 1
    }

    private func changeDisplayedQuote() {
        displayedQuoteIndex = (displayedQuoteIndex + 1) % quotes.count
        quoteLabel.text = quotes[displayedQuoteIndex]
    }

    private func animateDisplayedQuotes() {
        UIView.animate(withDuration: 2, delay: 1, options: .curveEaseIn) {
            self.hideQuoteLabel()
        } completion: { _ in
            self.changeDisplayedQuote()
            UIView.animate(withDuration: 2, delay: 0, options: .curveEaseOut) {
                self.showQuoteLabel()
            } completion: { _ in
                self.animateDisplayedQuotes()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        animateDisplayedQuotes()
    }
}
