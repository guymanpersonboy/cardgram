//
//  ScanViewController.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 12/6/21.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import AVFoundation

class ScanViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    let db = Firestore.firestore()
    var cameraError = false

    // instance variables
    private var captureSession: AVCaptureSession!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    private var scannedUID = ""

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        beginAVCapture()
    }

    // end capture when leaving view
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Stop scanning. Leaving view.")
        if (!cameraError) {
            print("Stop scanning. Leaving view.")
            self.captureSession.stopRunning()
            self.previewLayer.removeFromSuperlayer()
        }
    }

    // MARK: AV CAPTURE

    // set up the AVCaptureSession and AVCaptureVideoPreviewLayer
    private func beginAVCapture() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            print("Device not applicable for video processing!")
            showCameraErrorAlert("Device not applicable for video processing!")
            return
        }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            print("Your device cannot give video input!")
            showCameraErrorAlert("Your device cannot give video input!")
            return
        }
        self.captureSession = AVCaptureSession()
        
        if self.captureSession.canAddInput(videoInput) {
            self.captureSession.addInput(videoInput)
        } else {
            print("Your device cannot add input in capture session!")
            return
        }
        let metadataOutput = AVCaptureMetadataOutput()
        
        if self.captureSession.canAddOutput(metadataOutput) {
            self.captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr, .ean8, .ean13, .pdf417, .upce, .code128, .code39, .code39Mod43, .code93, .interleaved2of5, .itf14, .upce]
        } else {
            print("Your device cannot add output in capture session!")
            return
        }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer.frame = self.view.layer.bounds
        self.previewLayer.videoGravity = .resizeAspectFill
        self.view.layer.addSublayer(self.previewLayer)
        print("Scanning")
        self.captureSession.startRunning()
    }

    // MARK: SCANNED OUTPUT

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

        print("Stop Scanning")
        captureSession.stopRunning()

        if let first = metadataObjects.first {
            guard let readableObject = first as? AVMetadataMachineReadableCodeObject else {
                return
            }
            guard let stringValue = readableObject.stringValue else {
                return
            }
            self.scannedUID = stringValue
            print(scannedUID)
            // add scannedUID's account as a friend
            self.db.collection("users").document(scannedUID).getDocument { (document, error) in
                if let document = document, document.exists {
                    self.addFriend(firstName: document["firstName"] as! String, lastName: document["lastName"] as! String)
                } else {
                    self.showNotSavedAlert("Invalid code.")
                }
            }
        } else {
            showNotSavedAlert("Not able to read the code: May need a clearer image.")
        }
    }

    // MARK: ADD FRIEND

    private func addFriend(firstName: String, lastName: String) {
        let selfDocument = self.db.collection("users").document(Auth.auth().currentUser!.uid)
        selfDocument.getDocument { [self] (document, error) in
            if let document = document, document.exists {
                var friends = document["friends"] as! [[String : String]]
                let newEntry = ["firstName" : firstName,
                                "lastName" : lastName,
                                "uid" : self.scannedUID]

                // check if friend already exists
                for f in friends {
                    if f == newEntry {
                        showNotSavedAlert("Cannot add same friend twice.")
                        return
                    }
                }

                friends.append(newEntry)
                // update Firestore data to add friend
                selfDocument.updateData([
                    "friends" : friends
                ]) { error in
                    if error != nil {
                        self.showNotSavedAlert("Failed to add friend.")
                        return
                    }
                }

                let friendsCount = document["friendsCount"] as! Int
                // increment Firestore friendCount
                selfDocument.updateData([
                    "friendsCount" : (friendsCount + 1)
                ]) { error in
                    if error != nil {
                        self.showNotSavedAlert("Friend Count failed to update.")
                    } else {
                        self.showSuccessSavedAlert("Friend successfully added.")
                    }
                }
            } // if let document
        }
    }

    // MARK: ALERTS
    
    // Inform the user that they dont have a viable camera
    func showCameraErrorAlert(_ message: String) {
        cameraError = true
        let alert = UIAlertController(title: "Error",
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true)
    }
    
    // Inform the user that the save was not successful.
    // And restart the AV Capture
    func showNotSavedAlert(_ message: String) {
        let alert = UIAlertController(title: "Error",
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {_ in
            print("Scanning")
            self.captureSession.startRunning()
        }))
        present(alert, animated: true)
    }
    
    // Inform the user that the save was successful.
    // And restart the AV Capture
    func showSuccessSavedAlert(_ message: String) {
        let alert = UIAlertController(title: "Success",
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
            print("Scanning")
            self.captureSession.startRunning()
        }))
        present(alert, animated: true)
    }

}
