//
//  FontViewController.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 11/4/21.
//

import UIKit

// Font view controller allows user to pick a font from a list of available fonts.
class FontViewController: ViewController, UIFontPickerViewControllerDelegate {

    var cardEditorDelegate: EditViewController?

    // Edit the font in the card when user selects a card.
    func fontPickerViewControllerDidPickFont(_ viewController: UIFontPickerViewController) {
        let descriptor = viewController.selectedFontDescriptor!
        cardEditorDelegate?.edit(textFontStyle: UIFont(descriptor: descriptor, size: 8).fontName)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? UIFontPickerViewController {
            destination.delegate = self
        }
    }
}
