//
//  TableSignOutCell.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 11/7/21.
//

import UIKit

class TableSignOutCell: UITableViewCell {

    @IBOutlet weak var signOutLabel: UILabel!
    @IBOutlet weak var signOutButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
