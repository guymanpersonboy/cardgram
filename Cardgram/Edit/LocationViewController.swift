//
//  AddressViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/1/21.
//

import UIKit

// View controller allows the user to change the location/address present in the card.
class LocationViewController: ViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    var cardEditorDelegate: EditViewController?

    @IBOutlet weak var scrollView: UIScrollView!
    // list of states available to choose from.
    let states: [String] = ["", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
                            "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
                            "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
                            "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
                            "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    @IBOutlet weak var statePicker: UIPickerView!
    @IBOutlet weak var addressInputGroup: InputGroup!
    @IBOutlet weak var cityInputGroup: InputGroup!
    @IBOutlet weak var zipCodeInputGroup: InputGroup!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        statePicker.dataSource = self
        statePicker.delegate = self
        addressInputGroup.textField.delegate = self
        addressInputGroup.textField.addTarget(self, action: #selector(addressTextFieldEditingChanged(_:)), for: .editingChanged)
        addressInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        cityInputGroup.textField.delegate = self
        cityInputGroup.textField.addTarget(self, action: #selector(cityTextFieldEditingChanged(_:)), for: .editingChanged)
        cityInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        zipCodeInputGroup.textField.delegate = self
        zipCodeInputGroup.textField.addTarget(self, action: #selector(zipCodeTextFieldEditingChanged(_:)), for: .editingChanged)
        zipCodeInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)

        addressInputGroup.textField.text = cardEditorDelegate?.cardView.address
        cityInputGroup.textField.text = cardEditorDelegate?.cardView.city
        zipCodeInputGroup.textField.text = cardEditorDelegate?.cardView.zipCode
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        statePicker.selectRow(cardEditorDelegate?.cardView.stateIndex ?? 0, inComponent: 0, animated: true)
    }

    @objc func scrollToTop(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = UIFont(name: "WorkSans-Regular", size: label.font.pointSize)
        label.text = states[row]
        label.textAlignment = .center

        return label
    }

    // Update the location using the address, city, and zip code.
    func updateLocation() -> Void {
        let stateIndex = statePicker.selectedRow(inComponent: 0)
        cardEditorDelegate?.cardView.setLocation(stateIndex: stateIndex, state: states[stateIndex], address: addressInputGroup.textField.text ?? "", city: cityInputGroup.textField.text ?? "", zipCode: zipCodeInputGroup.textField.text ?? "")
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateLocation()
    }

    @objc func addressTextFieldEditingChanged(_ sender: Any) {
        updateLocation()
    }

    @objc func cityTextFieldEditingChanged(_ sender: Any) {
        updateLocation()
    }

    @objc func zipCodeTextFieldEditingChanged(_ sender: Any) {
        updateLocation()
    }
}
