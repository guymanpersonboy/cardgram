//
//  TablePasswordCell.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 11/7/21.
//

import UIKit

class TablePasswordCell: UITableViewCell {

    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
