//
//  SettingsPasswordViewController.swift
//  Cardgram
//
//  Created by Nandu Vudumula on 10/21/21.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

// View responsible for allowing users to change their password.
class SettingsPasswordViewController: ViewController {
    let db = Firestore.firestore()

    private var completePassword = false

    @IBOutlet weak var newPasswordInputGroup: InputGroup!
    @IBOutlet weak var newConfirmPasswordInputGroup: InputGroup!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func savePassword(_ sender: Any) {
        saveNewPassword()
    }

    // Saves a new password to the Firebase authentication.
    func saveNewPassword() {
        if (newPasswordInputGroup.textField.text == newConfirmPasswordInputGroup.textField.text) {
            Auth.auth().currentUser?.updatePassword(to: newPasswordInputGroup.textField.text!) { error in
                if error != nil {
                    self.showChangedPasswordAlert(successFail: "Failure", message: "Password was not updated")
                    return
                }
                self.showChangedPasswordAlert(successFail: "Success", message: "Password has been updated")
            }
        }
        else {
            showErrorNewPasswordAlert(message: "Passwords do not match")
        }
    }

    // Display any errors for changing passwords.
    func showErrorNewPasswordAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in self.completePassword = false }))
        present(alert, animated: true)
    }

    // Inform the user that the password changed successfully.
    func showChangedPasswordAlert(successFail: String, message: String) {
        let alert = UIAlertController(title: "\(successFail)", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in self.completePassword = true }))
        present(alert, animated: true)
    }
}
