//
//  FriendsViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/17/21.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import UIKit

class FriendsViewController: ViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet private weak var friendsTableView: UITableView!

    var uid: String! = ""
    private var friendsList: [[String: Any]] = []
    private var selectedFriendUid: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        friendsTableView.delegate = self
        friendsTableView.dataSource = self
    }
    
    // make sure friends list is up to date
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showSpinner()
        Firestore.firestore()
            .collection("users")
            .document(uid)
            .getDocument { (document, error) in
                if let document = document, document.exists {
                    guard let friends = document.get("friends") as? [[String: Any]] else {
                        print("\nError: failed to retrieve list of friends.")
                        return
                    }
                    self.friendsList = friends
                    if self.friendsList.count == 0 {
                        self.hideSpinner()
                    }
                    self.friendsTableView.reloadData()
                }
            }
    }
    
    // MARK: TABLE VIEW

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let friendCell = tableView.dequeueReusableCell(
            withIdentifier: "FriendCell", for: indexPath
        ) as! UserTableViewCell

        Storage.storage()
            .reference()
            .child("images/\(friendsList[indexPath.row]["uid"]!)/profilepicture")
            .downloadURL { (url, error) in
                guard let url = url, error == nil else {
                    return
                }

                URLSession.shared.dataTask(with: url, completionHandler: { data, _, error in
                    guard let data = data, error == nil else {
                        return
                    }
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        friendCell.profilePictureImageView.contentMode = UIView.ContentMode.scaleAspectFill
                        friendCell.profilePictureImageView.layer.cornerRadius = friendCell.profilePictureImageView.frame.height / 2
                        friendCell.profilePictureImageView.layer.masksToBounds = false
                        friendCell.profilePictureImageView.clipsToBounds = true
                        friendCell.profilePictureImageView.layer.borderWidth = 2
                        friendCell.profilePictureImageView.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
                        friendCell.profilePictureImageView.image = image

                        if indexPath.row == self.friendsList.count - 1 {
                            self.hideSpinner()
                        }
                    }
                }).resume()
            }

        if friendsList[indexPath.row]["uid"] as! String != Auth.auth().currentUser!.uid {
            friendCell.nameLabel.textColor = UIColor(named: "AccentColor")
        } else {
            friendCell.nameLabel.textColor = nil
        }

        friendCell.nameLabel.text = "\(friendsList[indexPath.row]["firstName"]!) \(friendsList[indexPath.row]["lastName"]!)"

        return friendCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if friendsList[indexPath.row]["uid"] as! String != Auth.auth().currentUser!.uid {
            performSegue(withIdentifier: "UserProfileSegue", sender: self)
        }
    }
    
    // MARK: NAVIGATION

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? UserProfileViewController {
            let index = friendsTableView.indexPathForSelectedRow!.row
            destination.firstName = friendsList[index]["firstName"] as? String
            destination.lastName = friendsList[index]["lastName"] as? String
            destination.uid = friendsList[index]["uid"] as? String
        }
    }
}
