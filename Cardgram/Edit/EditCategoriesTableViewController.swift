//
//  EditCategoriesTableViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/1/21.
//

import FirebaseAuth
import FirebaseStorage
import UIKit

// Table view that contains all the table view categories to edit the card.
class EditCategoriesTableViewController: UITableViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    var cardEditorDelegate: EditViewController?

    @IBAction func watermarkButtonTouchUpInside(_ sender: Any) {
        let watermarkPicker = UIImagePickerController()
        watermarkPicker.sourceType = .photoLibrary
        watermarkPicker.delegate = self
        watermarkPicker.allowsEditing = true
        present(watermarkPicker, animated: true)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        cardEditorDelegate?.edit(watermark: nil)
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        cardEditorDelegate?.edit(watermark: info[UIImagePickerController.InfoKey.editedImage] as? UIImage)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ProfessionalNameViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? PositionViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? LocationViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? ContactInformationViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? PickColorViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? FontViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? TextColorViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }

        if let destination = segue.destination as? AccentColorViewController {
            destination.cardEditorDelegate = cardEditorDelegate
        }
    }
}
