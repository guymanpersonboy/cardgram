//
//  PositionViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/1/21.
//

import UIKit

// View controller allows the user to change their professional position.
class ContactInformationViewController: ViewController, UITextFieldDelegate {
    var cardEditorDelegate: EditViewController?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailAddressInputGroup: InputGroup!
    @IBOutlet weak var phoneNumberInputGroup: InputGroup!
    @IBOutlet weak var websiteInputGroup: InputGroup!

    override func viewDidLoad() {
        super.viewDidLoad()

        emailAddressInputGroup.textField.delegate = self
        phoneNumberInputGroup.textField.delegate = self
        websiteInputGroup.textField.delegate = self

        emailAddressInputGroup.textField.addTarget(self, action: #selector(emailAddressTextFieldEditingChanged(_:)), for: .editingChanged)
        phoneNumberInputGroup.textField.addTarget(self, action: #selector(phoneNumberTextFieldEditingChanged(_:)), for: .editingChanged)
        websiteInputGroup.textField.addTarget(self, action: #selector(websiteTextFieldEditingChanged(_:)), for: .editingChanged)
        emailAddressInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        phoneNumberInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        websiteInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)

        websiteInputGroup.textField.text = cardEditorDelegate?.cardView.website
        phoneNumberInputGroup.textField.text = cardEditorDelegate?.cardView.phoneNumber
        emailAddressInputGroup.textField.text = cardEditorDelegate?.cardView.emailAddress
    }

    @objc func scrollToTop(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }

    @objc func emailAddressTextFieldEditingChanged(_ sender: Any) {
        cardEditorDelegate?.edit(emailAddress: emailAddressInputGroup.textField.text ?? "")
    }

    @objc func phoneNumberTextFieldEditingChanged(_ sender: Any) {
        cardEditorDelegate?.edit(phoneNumber: phoneNumberInputGroup.textField.text ?? "")
    }

    @objc func websiteTextFieldEditingChanged(_ sender: Any) {
        cardEditorDelegate?.edit(website: websiteInputGroup.textField.text ?? "")
    }
}
