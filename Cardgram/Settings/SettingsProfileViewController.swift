//
//  SettingsProfileViewController.swift
//  Cardgram
//
//  Created by Nandu Vudumula on 10/21/21.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseStorageUI
import Photos

class SettingsProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    let db = Firestore.firestore()
    let storage = Storage.storage().reference()
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadProfile()
        loadLabels()
    }
    
    // MARK: LOAD CURRENT PROFILE

    // load profile pic from Firebase Storage
    private func loadProfile() {
        Storage.storage()
            .reference()
            .child("images/\(Auth.auth().currentUser!.uid)/profilepicture")
            .downloadURL { (url, error) in
                guard let url = url, error == nil else {
                    return
                }

                URLSession.shared.dataTask(with: url, completionHandler: { data, _, error in
                    guard let data = data, error == nil else {
                        return
                    }
                    DispatchQueue.main.async { [self] in
                        let image = UIImage(data: data)
                        self.maskCircle()
                        profilePic.image = image
                    }
                }).resume()
            }
    }
    
    // update the labels on the Settings Page
    private func loadLabels() {
        self.db.collection("users").document(Auth.auth().currentUser!.uid).getDocument {
            (document, error) in
            if let document = document, document.exists {
                
                let firstName: String = document.get("firstName") as! String
                let lastName: String = document.get("lastName") as! String
                
                let bio: String = document.get("bio") as! String
                
                self.nameLabel.text = "\(firstName) \(lastName)"
                self.bioLabel.text = "\(bio)"
                
            }
        }
    }
    
    // MARK: TABLE VIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    let cellIdentifiers = [
        "ChangeProfileCell",
        "EditNameCell",
        "EditBioCell"
    ]
    let titleStrings = ["Change Profile Photo", "Edit Name", "Edit Bio"]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[indexPath.row], for: indexPath)
        cell.textLabel?.text = titleStrings[indexPath.row]
        cell.textLabel?.textColor = UIColor.systemIndigo
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            changeProfilePic()
        }
        if (indexPath.row == 1) {
            editName()
        }
        editBio()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: CHANGE PROFILE PIC

    // Allows user to change the profile picture.
    private func changeProfilePic() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }

    // Allows user to pick image from photo library.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)

        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        guard let imageData = image.pngData() else {
            return
        }
        uploadToStorage(imageData: imageData)
    }
    
    // MARK: UPLOAD TO STORAGE
    
    // upload newly set photo to Firebase storage
    func uploadToStorage(imageData: Data) {
        // upload to Firebase Storage as Data
        let ref = storage.child("images/\(Auth.auth().currentUser!.uid)/profilepicture")

        ref.putData(imageData, metadata: nil, completion: {_, error in
            guard error == nil else {
                print("\nError: failed to upload")
                return
            }

            ref.downloadURL(completion: { url, error in
                guard let url = url, error == nil else {
                    return
                }
                
                let urlString = url.absoluteString
                print("\nDownload URL: \(urlString)")

                self.loadProfile()
            })
        })
    }

    // make square(* must to make circle),
    // resize(reduce the kilobyte) and
    // fix rotation.
    public func maskCircle() {
        profilePic.contentMode = UIView.ContentMode.scaleAspectFill
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.layer.masksToBounds = false
        profilePic.clipsToBounds = true
    }
    
    // dismiss imagePicker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: EDIT NAME/BIO

    private func editName() {
        showEditNameAlert()
    }
    
    private func editBio() {
        showEditBioAlert()
    }
    
    // MARK: SHOW EDIT ALERTS

    // Displays an alert to edit the name of the user.
    func showEditNameAlert() {
        let alert = UIAlertController(title: "Edit Name", message: "Modify name below", preferredStyle: .alert
        )
        alert.addTextField { field in
            field.placeholder = "First Name"
            field.returnKeyType = .next
            field.keyboardType = .emailAddress
        }
        alert.addTextField { field in
            field.placeholder = "Last Name"
            field.returnKeyType = .next
            field.keyboardType = .emailAddress
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {_ in
            guard let fields = alert.textFields, fields.count == 2 else {
                return
            }
            let firstNameField = fields[0]
            let lastNameField = fields[1]
            guard let firstName = firstNameField.text, !firstName.isEmpty else {
                return
            }
            guard let lastName = lastNameField.text, !lastName.isEmpty else {
                return
            }
            self.db.collection("users").document(Auth.auth().currentUser!.uid).updateData([
                "firstName": firstName,
                "lastName": lastName
            ]) { err in
                if (err != nil)  {
                    self.showNotSavedAlert(attribute: "Name")
                } else {
                    self.loadLabels()
                    self.showSuccessSavedAlert(attribute: "Name")
                }
            }
        }))
        present(alert, animated: true)
    }

    // Displays an alert to edit the bio of the user.
    func showEditBioAlert() {
        let alert = UIAlertController(title: "Edit Bio", message: "Modify bio below", preferredStyle: .alert
        )
        alert.addTextField { field in
            field.placeholder = "Bio"
            field.returnKeyType = .next
            field.keyboardType = .emailAddress
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {_ in
            guard let fields = alert.textFields, fields.count == 1 else {
                return
            }
            let bioField = fields[0]
            guard let bio = bioField.text, !bio.isEmpty else {
                return
            }
            self.db.collection("users").document(Auth.auth().currentUser!.uid).updateData([
                "bio": bio
            ]) { error in
                if error != nil {
                    self.showNotSavedAlert(attribute: "Bio")
                } else {
                    self.loadLabels()
                    self.showSuccessSavedAlert(attribute: "Bio")
                }
            }
        }))
        present(alert, animated: true)
    }
    
    // MARK: SHOW SUCCESS/FAILURE ALERTS

    // Inform the user that the save was successful.
    func showSuccessSavedAlert(attribute: String) {
        let alert = UIAlertController(title: "Success",
                                      message: "\(attribute) successfully saved.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true)
    }

    // Inform the user that the save was not successful.
    func showNotSavedAlert(attribute: String) {
        let alert = UIAlertController(title: "Error",
                                      message: "\(attribute) was not successfully saved. Please try again.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    // MARK: TEXT FIELD FUNCTIONS

    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
