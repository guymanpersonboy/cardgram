//
//  SettingsViewController.swift
//  Cardgram
//
//  Created by Nandu Vudumula on 10/21/21.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseStorageUI
import Photos


// View represents the settings page used to change user preferences.
class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let db = Firestore.firestore()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }

    // MARK: TABLE VIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    let cellIdentifiers = [
        "SettingsProfileCell",
        "SettingsPasswordCell",
        "SettingsSignOutCell"
    ]
    let labelStrings = ["Edit Profile", "Change Password", "Sign Out"]

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        // Custom cells are returned by a specific row.
        if (row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[row], for: indexPath) as! TableProfileCell
            cell.profileLabel?.text = labelStrings[row]
            cell.profileLabel.textColor = UIColor.systemIndigo
            cell.profileButton.setTitle("", for: .normal)
            return cell
        }
        if (row == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[row], for: indexPath) as! TablePasswordCell
            cell.passLabel?.text = labelStrings[row]
            cell.passLabel.textColor = UIColor.systemIndigo
            cell.passButton.setTitle("", for: .normal)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[row], for: indexPath) as! TableSignOutCell
        cell.signOutLabel?.text = labelStrings[row]
        cell.signOutLabel.textColor = UIColor.systemIndigo
        cell.signOutButton.setTitle("", for: .normal)
        return cell
    }
    
    // Only the Sign Out cell does some special segue
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 2) {
            signOut()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: SIGN OUT
    
    // Signs out the currently signed in user.
    func signOut() {
        do {
            try Auth.auth().signOut()
            showSignoutAlert(status: "Successful", message: "You have been signed out")
        } catch let signOutError as NSError {
            showSignoutAlert(status: "Unsuccessful", message: signOutError as! String)
        }
    }

    // Inform the user that the sign out was successful.
    func showSignoutAlert(status: String, message: String) {
        let alert = UIAlertController(title: "\(status)", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:
            {(action) in self.proceedToAuthenticationView() }))
        present(alert, animated: true)
    }

    // Changes the root view controller to the authentication view.
    func proceedToAuthenticationView() -> Void {
        let authenticationStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let authenticationViewController = authenticationStoryboard.instantiateViewController(identifier: "AuthenticationNavigationController")

        view.window?.rootViewController = authenticationViewController
        view.window?.makeKeyAndVisible()
    }

}
