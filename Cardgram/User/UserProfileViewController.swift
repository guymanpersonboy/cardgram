//
//  FriendProfileViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/17/21.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import Cosmos
import TinyConstraints

class UserProfileViewController: ViewController, UIScrollViewDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    let db = Firestore.firestore()
    let storage = Storage.storage().reference()

    var firstName: String!
    var lastName: String!
    var uid: String!
    var ratedBefore = false

    // views
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var profilePic: UIImageView!
    // buttons
    @IBOutlet weak var friendButton: UIButton!
    // user labels
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var friendsCountLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    
    var friendsCount: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "\(firstName!) \(lastName!)"
        let otherDocument = self.db.collection("users").document(uid)
        
        cosmosView.settings.fillMode = .half
        // set action on select star
        cosmosView.didTouchCosmos = { rating in
            otherDocument.getDocument(completion: { (document, error) in
                if let document = document, document.exists {
                    let ratingSum = document["ratingSum"] as! Double
                    let ratedCount = document["ratedCount"] as! Double
                    var peopleRated = document["peopleRated"] as! [String : Double]
                    let selfUid = Auth.auth().currentUser!.uid

                    if self.ratedBefore {
                        // update previous rating
                        let previousRating =  peopleRated[selfUid]
                        peopleRated[selfUid] = rating
                        let updatedRating = (ratingSum - previousRating!) + rating
                        otherDocument.updateData([
                            "ratingSum" : updatedRating,
                            "peopleRated" : peopleRated,
                            "averageRating" : updatedRating / ratedCount
                        ]) { error in
                            if error != nil {
                                self.showNotSavedAlert("Failed to update rating. Please reload page.")
                            }
                        }
                        
                        return
                    }

                    // add new rating
                    peopleRated[selfUid] = rating
                    otherDocument.updateData([
                        "ratingSum" : ratingSum + rating,
                        "ratedCount" : ratedCount + 1,
                        "averageRating" : (ratingSum + rating) / (ratedCount + 1),
                        "peopleRated" : peopleRated
                    ]) { error in
                        if error != nil {
                            self.showNotSavedAlert("Failed to save rating. Please reload page.")
                            return
                        }
                        self.ratedBefore = true
                    }
                } // if let document
            })
        }

    }

    // refresh the screen with new data before displaying it
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showSpinner()
        loadProfile()
        loadLabels()
        setButton()
        Firestore.firestore()
            .collection("cards")
            .document(uid)
            .getDocument { (document, error) in
                if let document = document, document.exists {
                    self.cardView.deserialize(serialization: document)
                }
            }
    }

    // function to set the card in the view with zooming
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return cardView
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: LOAD CURRENT PROFILE
    
    // load profile pic from Firebase Storage
    private func loadProfile() {
        Storage.storage()
            .reference()
            .child("images/\(uid!)/profilepicture")
            .downloadURL { (url, error) in
                guard let url = url, error == nil else {
                    return
                }

                URLSession.shared.dataTask(with: url, completionHandler: { data, _, error in
                    guard let data = data, error == nil else {
                        return
                    }
                    DispatchQueue.main.async { [self] in
                        let image = UIImage(data: data)
                        maskCircle()
                        profilePic.image = image
                        self.hideSpinner()
                    }
                }).resume()
            }
    }
    
    // make square(* must to make circle),
    // resize(reduce the kilobyte) and
    // fix rotation.
    public func maskCircle() {
        profilePic.contentMode = UIView.ContentMode.scaleAspectFill
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.layer.masksToBounds = false
        profilePic.clipsToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
    }

    // update the labels on the Home Page
    private func loadLabels() {
        let db = Firestore.firestore()
        db.collection("users").document(uid).getDocument {
            (document, error) in
            if let document = document, document.exists {
                let friendsCount: Int = document.get("friendsCount") as! Int
                self.friendsCountLabel.text = "\(friendsCount)"

                let averageRating: Double = document.get("averageRating") as! Double
                self.ratingLabel.text = String(format: "%.1f", averageRating)
                
                let name = "\(self.firstName ?? "") \(self.lastName ?? "")"
                let bio: String = document.get("bio") as! String
                let date: String = document.get("joiningDate") as! String

                self.nameLabel.text = name
                self.bioLabel.text = "\(bio)"
                self.dateLabel.text = "\(date)"

                let peopleRated = document.get("peopleRated") as! [String : Double]

                for k in peopleRated.keys {
                    if k == Auth.auth().currentUser!.uid {
                        self.ratedBefore = true
                    }
                }
            }
        }
    }

    // MARK: FRIEND BUTTON

    // set the button to either Add Friend or Remove Friend
    private func setButton() {
        let selfDocument = self.db.collection("users").document(Auth.auth().currentUser!.uid)
        selfDocument.getDocument { [self] (document, error) in
            if let document = document, document.exists {
                let friends = document["friends"] as! [[String : String]]
                for friend in friends {
                    if friend["uid"] == uid {
                        friendButton.setTitle("Remove Friend", for: .normal)
                        friendButton.setImage(UIImage(systemName: "person.badge.minus"), for: .normal)
                        return
                    }
                }

                friendButton.titleLabel!.text = "Add Friend"
                friendButton.setImage(UIImage(systemName: "person.badge.plus"), for: .normal)
            }
        }
    }

    // MARK: MANAGE FRIEND

    @IBAction func friendButtonPressed(_ sender: Any) {
        if friendButton.titleLabel?.text == "Add Friend" {
            addFriend()
        } else if friendButton.titleLabel?.text == "Remove Friend" {
            removeFriend()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FriendsViewController {
            destination.uid = uid
            destination.title = "\(firstName!)'s Friends"
        }
    }

    private func addFriend() {
        let selfDocument = self.db.collection("users").document(Auth.auth().currentUser!.uid)
        selfDocument.getDocument { [self] (document, error) in
            if let document = document, document.exists {
                var friends = document["friends"] as! [[String : String]]
                friends.append(["firstName" : self.firstName,
                                "lastName" : self.lastName,
                                "uid" : self.uid])
                let friendsCount = document["friendsCount"] as! Int

                // update firestore data for this user
                selfDocument.updateData([
                    "friends" : friends
                ]) { error in
                    if error != nil {
                        self.showNotSavedAlert("Failed to add friend.")
                        return
                    }
                }
                selfDocument.updateData([
                    "friendsCount" : (friendsCount + 1)
                ]) { error in
                    if error != nil {
                        self.showNotSavedAlert("Friend Count failed to update.")
                    } else {
                        friendButton.setTitle("Remove Friend", for: .normal)
                        friendButton.setImage(UIImage(systemName: "person.badge.minus"), for: .normal)
                    }
                }
            } // if let document
        }
    }

    private func removeFriend() {
        let selfDocument = self.db.collection("users").document(Auth.auth().currentUser!.uid)
        selfDocument.getDocument { [self] (document, error) in
            if let document = document, document.exists {
                var friends = document["friends"] as! [[String : String]]
                let tempEntry: [String : String] = [
                                "firstName" : self.firstName,
                                "lastName" : self.lastName,
                                "uid" : self.uid
                ]
                guard let entryIndex = friends.firstIndex(of: tempEntry) else {
                    self.showNotSavedAlert("Failed to remove friend.")
                    return
                }
                friends.remove(at: entryIndex)
                let friendsCount = document["friendsCount"] as! Int


                // update firestore data for this user
                selfDocument.updateData([
                    "friends" : friends
                ]) { error in
                    if error != nil {
                        self.showNotSavedAlert("Failed to remove friend.")
                        return
                    }
                }
                selfDocument.updateData([
                    "friendsCount" : (friendsCount - 1)
                ]) { error in
                    if error != nil {
                        self.showNotSavedAlert("Friend Count failed to update.")
                    } else {
                        friendButton.setTitle("Add Friend", for: .normal)
                        friendButton.setImage(UIImage(systemName: "person.badge.plus"), for: .normal)
                    }
                }
            } // if let document
        }
    }
    
    // MARK: ALERTS
    
    // Inform the user that the save was not successful.
    func showNotSavedAlert(_ message: String) {
        let alert = UIAlertController(title: "Error",
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    // Inform the user that the save was successful.
    func showSuccessSavedAlert(_ message: String) {
        let alert = UIAlertController(title: "Success",
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
}
