//
//  SignInViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 10/13/21.
//

import FirebaseAuth
import UIKit

// View responsible for signing in an existing user.
class SignInViewController: ViewController {
    // Fields required to sign in an existing user.

    @IBOutlet weak var emailAddressInputGroup: InputGroup!
    @IBOutlet weak var passwordInputGroup: InputGroup!

    // Button tapped on when user has finished the sign in form.
    @IBOutlet weak var signInButton: UIButton!

    // Error label used to display the status of the authentication process and
    // any errors if they exist.
    @IBOutlet weak var errorLabel: UILabel!

    // Displays a registration error to the user.
    func showError(errorMessage: String) -> Void {
        errorLabel.text = errorMessage
        errorLabel.isHidden = false
    }

    // Gets rid of the registration error.
    func clearError() -> Void {
        errorLabel.text = ""
        errorLabel.isHidden = true
    }

    // Checks whether a registration error exists.
    func errorExists() -> Bool {
        return errorLabel.text != "" && errorLabel.isHidden == false
    }

    // Signs in an existing user using their email address and password.
    func signInUser() -> Void {
        view.endEditing(true)
        Auth.auth().signIn(
            withEmail: emailAddressInputGroup.textField.text ?? "",
            password: passwordInputGroup.textField.text ?? ""
        ) { (result: AuthDataResult?, error: Error?) in
            self.hideSpinner()

            if error != nil {
                self.showError(errorMessage: error!.localizedDescription)
            } else {
                self.proceedToMainView()
            }
        }
    }

    // Transitions to the main view by replacing the root view controller.
    func proceedToMainView() -> Void {
        let tabBarStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
        let tabBarController = tabBarStoryboard.instantiateViewController(identifier: "TabBarController")

        view.window?.rootViewController = tabBarController
        view.window?.makeKeyAndVisible()
    }

    override func viewDidLoad() -> Void {
        super.viewDidLoad()

        clearError()
    }

    // Handles tap on the sign in button.
    @IBAction func tappedSignInButton(_ sender: Any) -> Void {
        clearError()
        showSpinner()
        signInUser()
    }
}
