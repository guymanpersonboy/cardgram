//
//  RegisterViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 10/13/21.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import UIKit

// View responsible for registering a new user into the application.
class RegisterViewController: ViewController {
    let storage = Storage.storage().reference()
    let defaultImageName = "default-profile-picture"

    // Initial data used to register a user into the system.
    @IBOutlet private weak var firstNameInputGroup: InputGroup!
    @IBOutlet private weak var lastNameInputGroup: InputGroup!
    @IBOutlet private weak var emailAddressInputGroup: InputGroup!
    @IBOutlet private weak var passwordInputGroup: InputGroup!
    @IBOutlet weak var confirmPasswordInputGroup: InputGroup!

    // Button the user taps once the registration form is complete.
    @IBOutlet private weak var registerButton: UIButton!

    // Error label used to display errors in registration status.
    @IBOutlet private weak var errorLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        clearError()
    }

    // MARK: MANAGE ERROR
    
    // Displays a registration error to the user.
    func showError(errorMessage: String) {
        errorLabel.text = errorMessage
        errorLabel.isHidden = false
    }

    // Gets rid of the registration error.
    func clearError() {
        errorLabel.text = ""
        errorLabel.isHidden = true
    }

    // Checks whether a registration error exists.
    func errorExists() -> Bool {
        return errorLabel.text != "" && errorLabel.isHidden == false
    }
    
    // MARK: VALIDATE

    // Validates the registration fields based on certain parameters.
    func validateFields() -> Void {
        view.endEditing(true)

        // All fields must be filled in.
        if firstNameInputGroup.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameInputGroup.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            showError(errorMessage: "All fields must be filled in.")
            return
        }

        if confirmPasswordInputGroup.textField.text != passwordInputGroup.textField.text {
            showError(errorMessage: "Passwords do not match.")
            return
        }
    }

    // MARK: CREATE NEW USER

    // Creates a new user by contacting Firebase Authentication.
    // Signs in this new user once the user has been created.
    func createNewUser() -> Void {
        Auth.auth().createUser(
            withEmail: emailAddressInputGroup.textField.text!,
            password: passwordInputGroup.textField.text!
        ) { (result: AuthDataResult?, error: Error?) -> Void in
            if error != nil {
                self.hideSpinner()
                self.showError(errorMessage: error!.localizedDescription)
                return
            }
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"

            Firestore.firestore()
                .collection("users")
                .document(result!.user.uid)
                .setData([
                    "uid": result!.user.uid,
                    "firstName" : self.firstNameInputGroup.textField.text!,
                    "lastName": self.lastNameInputGroup.textField.text!,
                    "emailAddress": self.emailAddressInputGroup.textField.text!,
                    "friendsCount": 0,
                    "friends": [[String : String]](),
                    "bio": "New User.",
                    "ratingSum" : 0.0,
                    "ratedCount" : 0.0,
                    "peopleRated" : [String : Double](),
                    "averageRating" : 0.0,
                    "joiningDate" : dateFormatter.string(from: date)
            ]) { error in
                if error != nil {
                    self.showError(errorMessage: error!.localizedDescription)
                    self.hideSpinner()
                    return
                }
                // upload default image to storage
                guard let image = UIImage(named: self.defaultImageName) else {
                    self.showError(errorMessage: "Error: no system default image.")
                    self.hideSpinner()
                    return
                }
                guard let imageData = image.pngData() else {
                    self.showError(errorMessage: "Error: failed to convert image to Data")
                    self.hideSpinner()
                    return
                }
                let ref = self.storage.child("images/\(Auth.auth().currentUser!.uid)/profilepicture")
                
                ref.putData(imageData, metadata: nil, completion: { _, error in
                    self.hideSpinner()
                    guard error == nil else {
                        self.showError(errorMessage: error!.localizedDescription)
                        return
                    }
                    self.proceedToMainView()
                })
                // success
            }
        }
    }

    // Transition to the main view.
    func proceedToMainView() {
        let tabBarStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
        let tabBarController = tabBarStoryboard.instantiateViewController(identifier: "TabBarController")

        view.window?.rootViewController = tabBarController
        view.window?.makeKeyAndVisible()
    }

    // Handle a tap on the registration button.
    @IBAction func tappedRegisterButton(_ sender: Any) -> Void {
        clearError()
        showSpinner()
        validateFields()
        if errorExists() {
            hideSpinner()
            return
        }

        createNewUser()
    }
}
