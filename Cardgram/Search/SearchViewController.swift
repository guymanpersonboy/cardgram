//
//  SearchViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/29/21.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import UIKit

class SearchViewController: ViewController, UITableViewDelegate, UITableViewDataSource {
    
    let db = Firestore.firestore()
    
    @IBOutlet weak var searchInputGroup: InputGroup!
    @IBOutlet weak var usersTableView: UITableView!

    private var usersList: [[String: Any]] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchTextFieldEditingChanged(self)
        searchInputGroup.textField.addTarget(self, action: #selector(searchTextFieldEditingChanged(_:)), for: .editingChanged)

        usersTableView.delegate = self
        usersTableView.dataSource = self
    }

    // MARK: TABLE VIEW

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userCell = tableView.dequeueReusableCell(
            withIdentifier: "UserCell", for: indexPath
        ) as! UserTableViewCell

        Storage.storage()
            .reference()
            .child("images/\(usersList[indexPath.row]["uid"]!)/profilepicture")
            .downloadURL { (url, error) in
                guard let url = url, error == nil else {
                    return
                }

                URLSession.shared.dataTask(with: url, completionHandler: { data, _, error in
                    guard let data = data, error == nil else {
                        return
                    }
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        userCell.profilePictureImageView.contentMode = UIView.ContentMode.scaleAspectFill
                        userCell.profilePictureImageView.layer.cornerRadius = userCell.profilePictureImageView.frame.height / 2
                        userCell.profilePictureImageView.layer.masksToBounds = false
                        userCell.profilePictureImageView.clipsToBounds = true
                        userCell.profilePictureImageView.layer.borderWidth = 2
                        userCell.profilePictureImageView.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
                        userCell.profilePictureImageView.image = image
                    }
                }).resume()
            }

        userCell.nameLabel.text = "\(usersList[indexPath.row]["firstName"]!) \(usersList[indexPath.row]["lastName"]!)"

        return userCell
    }

    // MARK: QUERY

    @objc func searchTextFieldEditingChanged(_ sender: Any) {
        db.collection("users")
            .whereField("firstName", isGreaterThanOrEqualTo: searchInputGroup.textField.text!)
            .whereField("firstName", isLessThanOrEqualTo: searchInputGroup.textField.text! + "~")
            .getDocuments { (query, error) in
                self.usersList.removeAll()
                self.usersTableView.reloadData()

                if (query == nil || query!.isEmpty) {
                    print("Error: failed to query users or no users found.")
                    return
                }

                for document in query!.documents {
                    if document.data()["uid"] as! String != Auth.auth().currentUser!.uid {
                        self.usersList.append(document.data())
                    }
                }
                self.usersTableView.reloadData()
            }
    }

    // MARK: NAVIGATION
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? UserProfileViewController {
            let index = usersTableView.indexPathForSelectedRow!.row
            destination.firstName = usersList[index]["firstName"] as? String
            destination.lastName = usersList[index]["lastName"] as? String
            destination.uid = usersList[index]["uid"] as? String
        }
    }
}
