//
//  CardView.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/16/21.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import UIKit

class CardView: UIView {
    @IBOutlet private weak var nameLabel: UILabel!

    @IBOutlet private weak var roleLabel: UILabel!

    @IBOutlet private weak var locationLabel: UILabel!

    @IBOutlet private weak var emailAddressLabel: UILabel!

    @IBOutlet private weak var companyLabel: UILabel!

    @IBOutlet private weak var departmentLabel: UILabel!

    @IBOutlet private weak var phoneNumberLabel: UILabel!

    @IBOutlet private weak var websiteLabel: UILabel!

    @IBOutlet private weak var accentColorLabel: UILabel!

    @IBOutlet private weak var backgroundImageView: UIImageView!

    @IBOutlet private weak var watermarkImageView: UIImageView!

    private var textLabels: [UILabel] {
        get {
            return [
                nameLabel,
                roleLabel,
                locationLabel,
                emailAddressLabel,
                companyLabel,
                departmentLabel,
                phoneNumberLabel,
                websiteLabel
            ]
        }
    }
    
    // MARK: GET/SET DECLARATIONS

    public var courtesyTitleIndex: Int = 0
    public var courtesyTitle: String = ""
    public var firstName: String = "Paul"
    public var middleName: String = ""
    public var lastName: String = "Allen"
    // the name label getter/setter
    private(set) public var name: String {
        get {
            nameLabel.text ?? ""
        }
        set {
            nameLabel.text = newValue
        }
    }

    public func setName(courtesyTitleIndex: Int, courtesyTitle: String, firstName: String, middleName: String, lastName: String = "") {
        self.courtesyTitleIndex = courtesyTitleIndex
        self.courtesyTitle = courtesyTitle
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName

        var fullProfessionalName = "\(courtesyTitle)"
        if firstName != "" {
            fullProfessionalName += " \(firstName)"
        }
        if middleName != "" {
            fullProfessionalName += " \(middleName)"
        }
        if lastName != "" {
            fullProfessionalName += " \(lastName)"
        }
        self.name = fullProfessionalName
    }

    // the role label getter/setter
    public var role: String {
        get {
            roleLabel.text ?? ""
        }
        set {
            roleLabel.text = newValue
        }
    }

    public var stateIndex: Int = 0
    public var state: String = ""
    public var address: String = "358 EXCHANGE PLACE"
    public var city: String = ""
    public var zipCode: String = ""
    // the location label getter/setter
    private(set) public var location: String {
        get {
            locationLabel.text ?? ""
        }
        set {
            locationLabel.text = newValue
        }
    }

    public func setLocation(stateIndex: Int, state: String, address: String, city: String, zipCode: String) {
        self.stateIndex = stateIndex
        self.state = state
        self.address = address
        self.city = city
        self.zipCode = zipCode

        var location = ""
        if address != "" {
            location += address
        }
        if city != "" {
            if address != "" {
                location += ", "
            }
            location += city
        }
        if state != "" {
            if address != "" || city != "" {
                location += ", "
            }
            location += state
        }
        if zipCode != "" {
            if address != "" || city != "" || state != "" {
                location += ", "
            }
            location += zipCode
        }
        self.location = location
    }

    // the email address getter/setter
    public var emailAddress: String {
        get {
            emailAddressLabel.text ?? ""
        }
        set {
            emailAddressLabel.text = newValue
        }
    }

    // the company getter/setter
    public var company: String {
        get {
            companyLabel.text ?? ""
        }
        set {
            companyLabel.text = newValue
        }
    }

    // the department getter/setter
    public var department: String {
        get {
            departmentLabel.text ?? ""
        }
        set {
            departmentLabel.text = newValue
        }
    }

    // the phone number getter/setter
    public var phoneNumber: String {
        get {
            phoneNumberLabel.text ?? ""
        }
        set {
            phoneNumberLabel.text = newValue
        }
    }

    // the website getter/setter
    public var website: String {
        get {
            websiteLabel.text ?? ""
        }
        set {
            websiteLabel.text = newValue
        }
    }

    // the text font getter/setter
    public var textFontStyle: String {
        get {
            textLabels[0].font.fontName
        }
        set {
            for textLabel: UILabel in textLabels {
                textLabel.font = UIFont(name: newValue, size: textLabel.font.pointSize)
            }
        }
    }

    // the text color getter/setter
    public var textColor: UIColor {
        get {
            textLabels[0].textColor
        }
        set {
            for textLabel: UILabel in textLabels {
                textLabel.textColor = newValue
            }
        }
    }

    // the accent color label getter/setter
    public var accentColor: UIColor {
        get {
            accentColorLabel.backgroundColor ?? UIColor.white.withAlphaComponent(0.0)
        }
        set {
            accentColorLabel.backgroundColor = newValue
        }
    }

    public var backgroundIndex: Int = 0
    private var backgroundCase: CardBackground = .none
    // the backgound view getter/setter
    private(set) public var background: CardBackground {
        get {
            backgroundCase
        }
        set {
            backgroundCase = newValue
            backgroundImageView.image = UIImage(named: "\(newValue.rawValue).png")
        }
    }

    public func setBackgroundColor(backgroundIndex: Int, background: CardBackground) {
        self.backgroundIndex = backgroundIndex
        self.background = background
    }

    public var watermark: UIImage? {
        get {
            watermarkImageView.image
        }
        set {
            watermarkImageView.image = newValue
        }
    }

    // MARK: INIT FUNCTIONS

    private func configureView() {
        let viewFromXib = Bundle.main.loadNibNamed("CardView", owner: self, options: nil)![0]
            as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.configureView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        configureView()
    }

    // MARK: SERIALIZE FUNCTIONS

    private func serializeColor(color: UIColor) -> [String: CGFloat] {
        return [
            "red": CIColor(color: color).red,
            "green": CIColor(color: color).green,
            "blue": CIColor(color: color).blue,
            "alpha": CIColor(color: color).alpha
        ]
    }

    private func deserializeColor(serializedColor: [String: CGFloat]) -> UIColor {
        return UIColor(
            red: serializedColor["red"]!,
            green: serializedColor["green"]!,
            blue: serializedColor["blue"]!,
            alpha: serializedColor["alpha"]!
        )
    }

    private func serializeWatermark() -> String {
        guard watermark?.pngData() != nil else {
            return ""
        }

        let imageStoragePath = "images/\(Auth.auth().currentUser!.uid)/watermark"
        Storage.storage()
            .reference()
            .child(imageStoragePath)
            .putData(watermark!.pngData()!)
        return imageStoragePath
    }

    private func deserializeWatermark(serializedWatermark: String) {
        guard serializedWatermark != "" else {
            watermark = nil
            return
        }

        Storage.storage()
            .reference()
            .child(serializedWatermark)
            .downloadURL(completion: { (url, error) in
                guard let url = url, error == nil else {
                    return
                }

                URLSession.shared.dataTask(with: url, completionHandler: { data, _, error in
                    guard let data = data, error == nil else {
                        return
                    }
                    DispatchQueue.main.async { [self] in
                        watermark = UIImage(data: data)
                    }
                }).resume()
            })
    }

    // return an array of [String: Any] to get all card values
    public func serialize() -> [String: Any] {
        return [
            "courtesyTitleIndex": courtesyTitleIndex,
            "courtesyTitle": courtesyTitle,
            "firstName": firstName,
            "middleName": middleName,
            "lastName": lastName,
            "name": name,
            "role": role,
            "stateIndex": stateIndex,
            "state": state,
            "address": address,
            "city": city,
            "zipCode": zipCode,
            "location": location,
            "emailAddress": emailAddress,
            "company": company,
            "department": department,
            "phoneNumber": phoneNumber,
            "website": website,
            "textFontStyle": textFontStyle,
            "textColor": serializeColor(color: textColor),
            "accentColor": serializeColor(color: accentColor),
            "backgroundIndex": backgroundIndex,
            "background": background.rawValue,
            "watermark": serializeWatermark()
        ]
    }

    // set all card values using DocumentSnapshot
    public func deserialize(serialization: DocumentSnapshot) {
        courtesyTitleIndex = serialization["courtesyTitleIndex"] as! Int
        courtesyTitle = serialization["courtesyTitle"] as! String
        firstName = serialization["firstName"] as! String
        middleName = serialization["middleName"] as! String
        lastName = serialization["lastName"] as! String
        name = serialization["name"] as! String
        role = serialization["role"] as! String
        stateIndex = serialization["stateIndex"] as! Int
        state = serialization["state"] as! String
        address = serialization["address"] as! String
        city = serialization["city"] as! String
        zipCode = serialization["zipCode"] as! String
        location = serialization["location"] as! String
        emailAddress = serialization["emailAddress"] as! String
        company = serialization["company"] as! String
        department = serialization["department"] as! String
        phoneNumber = serialization["phoneNumber"] as! String
        website = serialization["website"] as! String
        textFontStyle = serialization["textFontStyle"] as! String
        textColor = deserializeColor(
            serializedColor: serialization["textColor"] as! [String : CGFloat]
        )
        accentColor = deserializeColor(
            serializedColor: serialization["accentColor"] as! [String : CGFloat]
        )
        backgroundIndex = serialization["backgroundIndex"] as! Int
        background = CardBackground(rawValue: serialization["background"] as! String)!
        deserializeWatermark(serializedWatermark: serialization["watermark"] as! String)
    }
}
