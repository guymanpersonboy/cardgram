//
//  PositionViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/1/21.
//

import UIKit

// View controller allows the user to change their professional position.
class PositionViewController: ViewController, UITextFieldDelegate {
    var cardEditorDelegate: EditViewController?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var companyInputGroup: InputGroup!
    @IBOutlet weak var departmentInputGroup: InputGroup!
    @IBOutlet weak var roleInputGroup: InputGroup!

    override func viewDidLoad() {
        super.viewDidLoad()

        companyInputGroup.textField.delegate = self
        departmentInputGroup.textField.delegate = self
        roleInputGroup.textField.delegate = self

        companyInputGroup.textField.addTarget(self, action: #selector(companyTextFieldEditingChanged(_:)), for: .editingChanged)
        departmentInputGroup.textField.addTarget(self, action: #selector(departmentTextFieldEditingChanged(_:)), for: .editingChanged)
        roleInputGroup.textField.addTarget(self, action: #selector(roleTextFieldEditingChanged(_:)), for: .editingChanged)
        companyInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        roleInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)
        departmentInputGroup.textField.addTarget(self, action: #selector(scrollToTop(_:)), for: .editingDidEnd)

        companyInputGroup.textField.text = cardEditorDelegate?.cardView.company
        roleInputGroup.textField.text = cardEditorDelegate?.cardView.role
        departmentInputGroup.textField.text = cardEditorDelegate?.cardView.department
    }

    @objc func scrollToTop(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }

    @objc func companyTextFieldEditingChanged(_ sender: Any) {
        cardEditorDelegate?.edit(company: companyInputGroup.textField.text ?? "")
    }

    @objc func departmentTextFieldEditingChanged(_ sender: Any) {
        cardEditorDelegate?.edit(department: departmentInputGroup.textField.text ?? "")
    }

    @objc func roleTextFieldEditingChanged(_ sender: Any) {
        cardEditorDelegate?.edit(role: roleInputGroup.textField.text ?? "")
    }
}
