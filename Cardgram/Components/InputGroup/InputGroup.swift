//
//  InputGroup.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 12/3/21.
//

import UIKit

@IBDesignable class InputGroup: UIView {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!

    @IBInspectable var name: String = "Label" {
        didSet {
            label.text = name
        }
    }

    @IBInspectable var isSecureTextEntry: Bool = false {
        didSet {
            textField.isSecureTextEntry = isSecureTextEntry
        }
    }

    @IBInspectable var isLabelHidden: Bool = false {
        didSet {
            label.isHidden = isLabelHidden
        }
    }

    @IBInspectable var placeholder: String = "" {
        didSet {
            textField.placeholder = placeholder
        }
    }

    private func configureView() {
        let bundle = Bundle(for: InputGroup.self)
        let inputGroupView = bundle.loadNibNamed(
            "InputGroup",
            owner: self,
            options: nil
        )?.first as! UIView

        textField.font = UIFont(name: "WorkSans-Regular", size: 15)

        inputGroupView.frame = bounds
        addSubview(inputGroupView)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        configureView()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        configureView()
    }
}
