//
//  CardBackgroundColor.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/16/21.
//

enum CardBackground: String {
    case black = "Black"
    case blue = "Blue"
    case forest = "Forest"
    case mint = "Mint"
    case navy = "Navy"
    case none = "None"
    case peach = "Peach"
    case pink = "Pink"
    case purple = "Purple"
    case red = "Red"
    case salmon = "Salmon"
    case seafoam = "Seafoam"
    case turquoise = "Turquoise"
}
