//
//  FriendTableViewCell.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 11/17/21.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
