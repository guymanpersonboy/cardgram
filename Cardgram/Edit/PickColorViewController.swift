//
//  PickColorViewController.swift
//  Cardgram
//
//  Created by Nandu Vudumula on 11/3/21.
//

import UIKit

// View controller that allows the user to change the colors in the card.
class PickColorViewController: ViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    var cardEditorDelegate: EditViewController?

    @IBOutlet weak var backgroundColorPicker: UIPickerView!

    // List of colors available for the background.
    let colors: [String] = ["None", "Black", "Navy", "Peach", "Purple", "Pink", "Salmon", "Blue", "Turquoise", "Seafoam", "Red", "Forest", "Mint"]

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundColorPicker.dataSource = self
        backgroundColorPicker.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        backgroundColorPicker.selectRow(cardEditorDelegate?.cardView.backgroundIndex ?? 0, inComponent: 0, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = UIFont(name: "WorkSans-Regular", size: label.font.pointSize)
        label.text = colors[row]
        label.textAlignment = .center

        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cardEditorDelegate?.cardView.setBackgroundColor(backgroundIndex: row, background: CardBackground(rawValue: colors[row])!)
    }
}
