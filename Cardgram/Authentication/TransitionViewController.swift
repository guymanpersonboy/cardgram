//
//  TransitionViewController.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 10/31/21.
//

import FirebaseAuth
import UIKit

// Intermediate view that will immediately be replaced with
// the appropriate view.
class TransitionViewController: ViewController {
    // Determines whether the user is authenticated and transitions to the correct view
    // based on it.
    override func viewDidLoad() -> Void {
        super.viewDidLoad()

        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                self.proceedToMainView()
            } else {
                self.proceedToAuthenticationView()
            }
        }
    }

    // Changes the root view controller to authentication view.
    private func proceedToAuthenticationView() -> Void {
        let authenticationStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let authenticationViewController = authenticationStoryboard.instantiateViewController(identifier: "AuthenticationNavigationController")

        view.window?.rootViewController = authenticationViewController
        view.window?.makeKeyAndVisible()
    }
    
    // Changes the root view controller to the main view.
    private func proceedToMainView() -> Void {
        let tabBarStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
        let tabBarController = tabBarStoryboard.instantiateViewController(identifier: "TabBarController")

        view.window?.rootViewController = tabBarController
        view.window?.makeKeyAndVisible()
    }
}
