//
//  ViewController.swift
//  Cardgram
//
//  Created by Prathamesh Shenoy on 12/3/21.
//

import UIKit

class ViewController: UIViewController {
    let spinnerViewController = SpinnerViewController()

    private func endEditingOnExternalTap() {
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: view,
            action: #selector(UIView.endEditing(_:))
        )
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        endEditingOnExternalTap()
    }

    public func showSpinner() {
        DispatchQueue.main.async { [self] in
            addChild(spinnerViewController)
            spinnerViewController.view.frame = view.frame
            view.addSubview(spinnerViewController.view)
            spinnerViewController.didMove(toParent: self)
        }
    }

    public func hideSpinner() {
        DispatchQueue.main.async { [self] in
            spinnerViewController.willMove(toParent: nil)
            spinnerViewController.view.removeFromSuperview()
            spinnerViewController.removeFromParent()
        }
    }
}
