legal

-----

icons8

For smartphone apps, please add a link to https://icons8.com in the about section or settings. 

Also, please credit our work in your App Store or Google Play description (something like "Icons by Icons8" is fine).

-----

logomakr

In order to legally use the file that you downloaded for free from LogoMakr.com, you must provide credit for the use of our creative copyright.

Files downloaded for free are bound to our copyright and therefore have royalties associated to them.

Free files may not be used commercially or printed.

Credit must be provided everywhere the file is used. Regardless of the platform or how the free file is used, credit is always required.

To provide credit add a link to LogoMakr.com anywhere the LogoMakr file is in use. Examples: Created my logo at LogoMakr.com. Designed my banner at LogoMakr.com. My graphics are from LogoMakr.com

logomakr.com/app/7PjgTT
