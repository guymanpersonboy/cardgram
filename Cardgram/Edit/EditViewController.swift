//
//  EditViewController.swift
//  Cardgram
//
//  Created by Christopher Carrasco on 10/30/21.
//

import FirebaseAuth
import FirebaseFirestore
import UIKit

// EditViewController allows the user to edit the card and the information in it.
class EditViewController: ViewController {
    @IBOutlet weak var cardView: CardView!

    public func edit(role: String) {
        cardView.role = role
    }

    public func edit(emailAddress: String) {
        cardView.emailAddress = emailAddress
    }

    public func edit(company: String) {
        cardView.company = company
    }

    public func edit(department: String) {
        cardView.department = department
    }

    public func edit(phoneNumber: String) {
        cardView.phoneNumber = phoneNumber
    }

    public func edit(website: String) {
        cardView.website = website
    }

    public func edit(textFontStyle: String) {
        cardView.textFontStyle = textFontStyle
    }

    public func edit(textColor: UIColor) {
        cardView.textColor = textColor
    }

    public func edit(accentColor: UIColor) {
        cardView.accentColor = accentColor
    }

    public func edit(watermark: UIImage?) {
        cardView.watermark = watermark
    }

    override func viewWillAppear(_ animated: Bool) {
        showSpinner()
        Firestore.firestore()
            .collection("cards")
            .document(Auth.auth().currentUser!.uid)
            .getDocument { (document, error) in
                if let document = document, document.exists {
                    self.cardView.deserialize(serialization: document)
                }

                self.hideSpinner()
            }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EditCategoriesTableViewController {
            destination.cardEditorDelegate = self
        }
    }

    @IBAction func saveButtonTouchUpInside(_ sender: Any) {
        Firestore.firestore()
            .collection("cards")
            .document(Auth.auth().currentUser!.uid)
            .setData(cardView.serialize())

        showCardSavedAlert()
    }

    // Show an alert to notify the user that the card was saved.
    func showCardSavedAlert() {
        let alert = UIAlertController(title: "Card Created", message: "Your card has been successfully saved", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler:
                                        nil))
        present(alert, animated: true)
    }
}
